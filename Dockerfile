FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env

WORKDIR /app

COPY src/WeatherApi.csproj ./
RUN dotnet restore WeatherApi.csproj
COPY . ./
RUN dotnet publish -c Release -o out
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
COPY --from=build-env app/out .

ENTRYPOINT ["dotnet", "WeatherApi.dll"]